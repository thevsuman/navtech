<?php
get_header();	
?>

<main>
	<div class="container default-page">
		<div class="category-body" style="margin: 60px 0;">
			<?php the_content(); ?>
		</div>
	</div>
</main>

<?php
get_footer();