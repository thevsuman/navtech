<?php

	// Utilities

	include( 'configure/utilities.php' );

	// CONFIG

	include( 'configure/configure.php' );

	// JAVASCRIPT & CSS

	include( 'configure/js-css.php' );

	// SHORTCODES

	include( 'configure/shortcodes.php' );

	// ACF

	include( 'configure/acf.php' );

	// BLOG

	include( 'configure/blog-func.php' );

	// HOOKS ADMIN

	// include( 'configure/custom-sidebar-function.php' );

	if( is_admin() )
	{
		include( 'configure/admin.php' );

	}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
