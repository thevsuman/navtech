<?php
// register ajax.php
//require get_template_directory() . '/inc/ajax.php';

if ( ! function_exists( 'get_partial' ) ) :

	/**
	 * Load given template with arguments as array.
	 * arguments.
	 * @see     get_template_part().
	 * @see     http://wordpress.stackexchange.com/a/103257
	 * @author  Julien Vasseur julien@poigneedemainvirile.com
	 */
	function get_partial( $slug = null, $name = null, array $params = array(), $prefix = null ) {
		global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

		/**
		 * Fires before the specified template part file is loaded.
		 *
		 * The dynamic portion of the hook name, `$slug`, refers to the slug name
		 * for the generic template part.
		 *
		 * @param string $slug The slug name for the generic template.
		 * @param string $name The name of the specialized template.
		 */
		do_action( "get_partial_{$slug}", $slug, $name );
		do_action( "get_template_part_{$slug}", $slug, $name );

		$templates = array();
		$name = (string) $name;
		if ( '' !== $name ) {
			$templates[] = "{$slug}-{$name}.php";
		}

		$templates[] = "{$slug}.php";

		$_template_file = locate_template( $templates, false, false );

		if ( is_array( $wp_query->query_vars ) ) {
			extract( $wp_query->query_vars, EXTR_SKIP );
		}

		if ( isset( $s ) ) {
			$s = esc_attr( $s );
		}

		if ( ! is_null( $prefix ) ) {
			$flags = EXTR_PREFIX_ALL;
			// ensure prefix doesn't end with an underscore, it is automatically added by extract()
			if ( '_' === $prefix[ strlen( $prefix ) - 1 ] ) {
				$prefix = substr( $prefix, 0, -1 );
			}
		} else {
			$flags = EXTR_PREFIX_SAME;
			$prefix = '';
		}

		extract( $params, $flags, $prefix );

		require( $_template_file );
	}

endif;

/** SHORTCODES FOR TABLE OF CONTENTS FOR CATEGORY STARTS */
// by default category does not do shortcode, enable with:
add_filter('term_description', 'shortcode_unautop');
add_filter('term_description', 'do_shortcode');
remove_filter('pre_term_description', 'wp_filter_kses');

    add_shortcode('TOC-categories', 'toc_all_categories_shortcode');
    function toc_all_categories_shortcode()
    {
        return cat_tree();
        $taxonomies = get_terms(array(
            'taxonomy' => 'category',
            'hide_empty' => false
        ));
        return toc_show_categories($taxonomies);
    }
    
    add_shortcode('TOC-context-categories', 'toc_context_categories_shortcode');
    
    function toc_context_categories_shortcode()
    {
        // get current context id
        global $wp_query;
        $id = $wp_query->get_queried_object_id();
        $cat_ids = array();
    
        if (is_single() || is_page()) {
            $cat_ids = wp_get_post_categories($id, array('fields' => 'ids'));
        } else if (is_category()) {
            $cat_ids[] = $id;
        }
    
        if (!empty($cat_ids)) {
            return cat_tree($cat_ids);
        }
    }
    
    function cat_tree($cat_id = 0, $depth = 0)
    {
        $output = '';
    
        $opts = array(
            'taxonomy' => 'category',
            'hide_empty' => false,
            'hierarchical' => true,
            'parent' => $cat_id
        );
    
        if (is_array($cat_id)) {
            unset($opts['parent']);
            $opts['include'] = $cat_id;
        }
    
        $categories = get_categories($opts);
    
        if (count($categories) > 0) {
            if ($depth == 0) {
                $output .= '
                <div class="table-of-content-template">
                <div class="table-of-content-heading"><em>Category</em><br>
                <i class="toggle-list"><i class="fas fa-align-right" "=""></i></i>
                </div>
                <div class="table-of-content-body">
                <ol>';
            } else {
                $output .= '<ol>';
            }
    
            foreach ($categories as $category) {
                //$output .=  '<option value="' . $category->cat_ID . '">' . $depth . $category->cat_name . '</option>';
                $output .= '<li><a href="' . get_category_link($category->cat_ID) . '">' . esc_html($category->cat_name) . '</a>';
                $output .=  cat_tree($category->cat_ID, $depth + 1);
                $output .= '</li>';
            }
    
            if ($depth == 0) {
                $output .= '</ol></div></div>';
            } else {
                $output .= '</ol>';
            }
        }
        return $output;
    }
    /** SHORTCODES FOR TABLE OF CONTENTS FOR CATEGORY ENDS */
    
    /** SHORTCODES FOR DYNAMIC TOC BASED ON HEADER STARTS */
    if (!function_exists('dynamic_TOC')) {
        function dynamic_TOC()
        {
            if (is_single() || is_page()) {
                global $wp_query;
                $post = $wp_query->get_queried_object();
                if ($post) {
                    $content = $post->post_content;
                    return generate_TOC($content);
                }
            } elseif (is_category()) {
                global $wp_query;
                $category = $wp_query->get_queried_object();
                if ($category) {
                    return generate_TOC($category->description);
                }
            }
        }
        add_shortcode('TOC-dynamic', 'dynamic_TOC');
    }
    
    if (!function_exists('generate_TOC')) {
        function generate_TOC($content)
        {
            $toc_html = '
        <div class="table-of-content-template">
        <div class="table-of-content-heading"><em>Table of Contents</em><br>
        <i class="toggle-list"><i class="fas fa-align-right" "=""></i></i>
        </div>
        <div class="table-of-content-body">
        <ol>';
    
            preg_match_all("/<(H2|H3|H4|H5|H6).*?>(.*?)<\/\\1>/i", $content, $matches);
            $TOC = array();
    
            // TODO: maybe use existing ID of heading if it exists
            if (isset($matches[2]) && count($matches[2])) {
                foreach ($matches[2] as $key => $match) {
                    $match = strip_tags($match);
                    if (!empty($match)) {
                        $TOC[] = [
                            'indent' => preg_replace('/[^0-9]/', '', $matches[1][$key]),
                            'header' => $matches[1][$key],
                            'id' => 'header_' . $key,
                            'title' => $match
                        ];
                    }
                }
            }
    
            if (!empty($TOC)) {
                $header_stash = [];
                $prev = 2;
                $level = 2;
                foreach ($TOC as $toc_element) {
                    $curr = (int) $toc_element['indent'];
                    if ($prev < $curr) {
                        $level++;
                        if (abs($prev - $curr) == 1) {
                            $toc_html .= '<ol>';
                        }
                    }
                    if ($prev > $curr) {
                        while ($level > 2) {
                            $level--;
                            if (abs($prev - $curr) == 1) {
                                $toc_html .= '</ol>';
                            }
                        }
                    }
                    $prev = $curr;
                    $toc_html .= ('<li><a href="#' . $toc_element['id'] . '"><span>' . $toc_element['title'] . '</span></a>');
                }
            } else {
                // no headings
                return '';
            }
    
            $toc_html .= '</ol></div></div>';
            return $toc_html;
        }
    }
    
    if (!function_exists('hook_content_add_id_to_headings')) {
        function hook_content_add_id_to_headings($content)
        {
    
            // Check if we're inside the main loop in a single post page.
            if (is_single() || is_page()) {
                return add_id_to_headings($content);
            }
    
            return $content;
        }
        add_filter('the_content', 'hook_content_add_id_to_headings');
    }
    
    if (!function_exists('hook_category_add_id_to_headings')) {
        function hook_category_add_id_to_headings($description)
        {
    
            // Check if we're inside the main loop in a single post page.
            if (is_archive()) {
                return add_id_to_headings($description);
            }
    
            return $description;
        }
        add_filter('category_description', 'hook_category_add_id_to_headings');
    }
    
    if (!function_exists('add_id_to_headings')) {
        function add_id_to_headings($content)
        {
            preg_match_all("/<(H2|H3|H4|H5|H6).*?>(.*?)<\/\\1>/i", $content, $matches);
    
            if (isset($matches[2]) && count($matches[2])) {
                foreach ($matches[2] as $key => $match) {
                    $new_string = str_replace('<' . $matches[1][$key], '<' . $matches[1][$key] . ' id="header_' . $key . '"', $matches[0][$key]);
                    $content = str_replace($matches[0][$key], $new_string, $content);
                }
            }
            return $content;
        }
    }
    /** SHORTCODES FOR DYNAMIC TOC BASED ON HEADER ENDS */

function register_my_menus() {
    register_nav_menus(
        array(
            'homepage-side-menu-1' => __( 'Homepage Sidebar 1' ),
            'homepage-side-menu-2' => __( 'Homepage Sidebar 2' )
        )
    );
}
add_action( 'init', 'register_my_menus' );