<?php
	function _add_javascript()
	{
		wp_enqueue_script('jquery-js','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true );
		wp_enqueue_script('owl-js','https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array(), null, true );
		wp_enqueue_script('equalHeight-js','https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js', array(), null, true );
		wp_enqueue_script('modernizr-js','https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), null, true );
		wp_enqueue_script('main-js', get_template_directory_uri() . '/js/main.js', array(), null, true );
	}
	add_action('wp_enqueue_scripts', '_add_javascript', 100);

	function _add_stylesheets()
	{
		wp_enqueue_style('poppins-css', 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,400;1,500;1,600;1,700&display=swap', array(), null, 'all');
		wp_enqueue_style('fontawesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css', array(), null,'all'); 
		wp_enqueue_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css', array(), null, 'all' );
		wp_enqueue_style('main-css', get_template_directory_uri() . '/css/style.css', array(), null, 'all' );
	}
	add_action('wp_enqueue_scripts', '_add_stylesheets');
