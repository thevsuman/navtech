<?php
get_header(); 

$obj = get_queried_object(); 
$cat_id = get_post_meta($the_query->ID, '_yoast_wpseo_primary_category', true);
$category = get_the_category($cat_id); 
$category_link = get_category_link( $category[0]->term_id );
?>
<main>
    <?php get_template_part( 'template_parts/section-category-hero' ); ?>
    <?php if (function_exists ('adinserter')) echo adinserter (1); ?>
    <div class="container pillar-body">
    	<?php echo category_description( $category_id ); ?>
    </div>
    <?php if (function_exists ('adinserter')) echo adinserter (2); ?>
    <div class="container">
	    <div class="cat-listing">
	    	<?php 
	    		$paged= (get_query_var('paged' )) ? get_query_var('paged'):1; 
				global $query_string;
				$cur_cat = get_cat_ID( single_cat_title("",false) );
				$myquery = wp_parse_args($query_string);
			    $args = array(
			    	'paged' => $paged,
	    			'number_posts'=>6,
			    	'cat' => $cur_cat,
			        'post_type' => 'post'
			    );
			    query_posts($args);
			    while ( have_posts() ) : the_post();
			    	
	    	    $image = wp_get_attachment_image_src( get_post_thumbnail_id($post_list->ID), 'post-list-row');
	            if(empty($cat_id) || $category == NULL) {
	                $perma_cat = get_post_meta($post_list->ID, '_category_permalink', true);
	                if (!empty($perma_cat)) {
	                    $cat_id = $perma_cat['category'];
	                    $category = get_category($cat_id);
	                } else {
	                    $categories = get_the_category();
	                    $category = $categories[0];
	                }
	            }
	            $category_name = $category->name;
	            $author_id=$post_list->post_author;
	    	?>
	    	<article class="item">
				<div class="img-con">
	                <a href="<?php echo get_permalink(); ?>" class="img-wrap">
	                    <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(); ?>" title="<?php echo get_the_title(); ?>" >
	                </a>
	                <div class="auth-cat">
	                	<span class="author">By <?php the_author_meta( 'user_nicename' , $author_id );?>&nbsp;&#8226;</span>
	                	<span class="category">&nbsp;<?php echo $category_name; ?></span>
	                </div>
	            </div>
	            <a href="<?php echo get_permalink(); ?>" class="title"><?php echo get_the_title(); ?>
	            </a>
			</article>
			<?php
				endwhile;
				 wp_pagenavi();
				wp_reset_query();  
			?>
	    </div>
	</div>
    
    <?php if(get_field('faq',$obj)): ?>
    <div class="category-bottom">
        <div class="container">
            <?php the_field('faq',$obj); ?>
        </div>   
    </div>
    <?php endif; ?>
</main>
<?php if (function_exists ('adinserter')) echo adinserter (4); ?>
<?php
get_footer();