<?php 
	$obj = get_queried_object();
	$image_bg_hero = get_field('hero_background',$obj);
	$logo_image = get_field('hero_logo_image',$obj);
    $size = 'full';
    $image_array = wp_get_attachment_image_src($image_bg_hero, $size);
    $logo_image_array = wp_get_attachment_image_src($logo_image, $size);
?>

<div class="hero" style="background-image: url('<?php echo $image_array[0]; ?>');">
	<div class="container">
		<?php if (get_field('hero_layout',$obj) == 'layout-default') : ?>
			<div class="inner">
				<h1><?php echo (get_field('hero_title')) ? get_field('hero_title') : get_the_title(); ?></h1>
				<?php if(get_field('hero_description',$obj)): ?>
					<p><?php the_field('hero_description',$obj); ?></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if (get_field('hero_layout',$obj) == 'layout-with-logo') :?>
			<div class="inner w-logo">
				<div class="left">
					<h1><?php echo (get_field('hero_title')) ? get_field('hero_title') : get_the_title(); ?></h1>
					<?php if(get_field('hero_description',$obj)): ?>
						<p><?php the_field('hero_description',$obj); ?></p>
					<?php endif; ?>
				</div>
				<div class="logo">
					<img src="<?php echo $logo_image_array[0]; ?>">
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>