	<footer>
		<div class="top">
			<div class="container">
				<div class="inner">
					<a href="<?php echo home_url(); ?>" class="logo">
						Navtech
					</a>
					<nav>
						<?php echo wp_nav_menu( array(
						    'menu'   => 'Header Menu'
						)); ?>
					</nav>
				</div>
			</div>
		</div>
	
	</footer>
	<?php wp_footer(); ?>
	</body>
</html>

	